%global app                     php
%global d_conf                  %{_sysconfdir}/%{app}.d

Name:                           meta-php
Version:                        1.0.1
Release:                        4%{?dist}
Summary:                        META-package for install and configure PHP
License:                        GPLv3

Source10:                       %{app}.custom.ini

Requires:                       php
Requires:                       php-gd
Requires:                       php-gmp
Requires:                       php-imap
Requires:                       php-intl
Requires:                       php-mbstring
Requires:                       php-mysqlnd
Requires:                       php-odbc
Requires:                       php-opcache
Requires:                       php-pdo
Requires:                       php-pecl-geoip
Requires:                       php-pecl-imagick
Requires:                       php-pecl-memcached
Requires:                       php-pecl-redis4
Requires:                       php-pecl-zip
Requires:                       php-pecl-uploadprogress
Requires:                       php-xml

%description
META-package for install and configure PHP.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_conf}/99-%{app}.custom.ini


%files
%config(noreplace) %{d_conf}/99-%{app}.custom.ini


%changelog
* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-4
- Update SPEC-file.

* Tue Apr 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-3
- UPD: Add "php-pecl-zip".

* Wed Apr 24 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-2
- UPD: Add "php-gmp".

* Fri Feb 15 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.1-1
- NEW: 1.0.1.

* Thu Jan 03 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- UPD: Remove "remi-release-29" requires.

* Wed Jan 02 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
